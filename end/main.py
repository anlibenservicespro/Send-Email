
from uuid import uuid4
from mailjet_rest import Client
import os
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

api_key = '87d7ee9c9158130f70b6fa0a417838ed'
api_secret = 'f0276ecc1bfb6038ee51184139104efc'
mailjet = Client(auth=(api_key, api_secret), version='v3.1')
origins = [
    "*",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


users = []
mails = []
tokens = []


@app.get('/verify')
def verify(email: str, password: str):
    
    for user in users:
        if user['email'] == email:
            if user['verify'] == True:
                return {
                    "message": "verified"
                }
            else:
                return {
                    "message": "no verified"
                }
        return {
                    "message": "not found"
                }
    return {
        "message": "error"
    }

@app.get('/request')
def request(email, password):
    token = uuid4().time
    users.append({
        "email": email,
        "password": password,
        "verify": False,
        "token": token
    })
    data = {
        'Messages': [
            {
                "From": {
                    "Email": "anliben.yayoi@gmail.com",
                    "Name": "joao"
                    },
                "To": [
                {
          "Email": email,
          "Name": "test"
        }
    ],
      "Subject": "Teste de envio de emails",
      "TextPart": "My first Mailjet email",
      "HTMLPart": f"<h3>Verifique seu email agora mesmo <a href='http://localhost:8000/confirm?email={email}&token={token}'>Verificar</a>!</h3><br />",
      "CustomID": "AppGettingStartedTest"
    }
    ]
    }
    result = mailjet.send.create(data=data)
    print(result)
    return {
        "message": "requested"
    }

@app.get('/confirm')
def confirm(email, token):
    
    users.clear()
    users.append({
        "email": email,
        "verify": True,
    })
    return {
        "message": "verified"
        }


